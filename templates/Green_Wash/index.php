<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
@ini_set('display_errors', '1');
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets
JHtml::_('jquery.framework', true, true);
//$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/css/foundation.min.css');
$doc->addStyleSheet(JURI::root(true).'/myCore/css/foundation/fonts/foundation-icons.css');
$doc->addScript('templates/'.$this->template.'/foundation/js/foundation.min.js');
$doc->addScript('templates/'.$this->template.'/foundation/js/vendor/modernizr.js');
$doc->addScript('templates/'.$this->template.'/js/lightSlider/jquery.lightSlider.min.js');
$doc->addScript('myCore/js/featherlight/featherlight.js');
$doc->addStyleSheet('myCore/js/featherlight/featherlight.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/lightSlider/lightSlider.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');
$doc->addScript('templates/'.$this->template.'/js/scripts.js');
// Add current user information
$user = JFactory::getUser();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body>
    <div id="top-link"></div>
    <header id="header">
        <div class="row fullWidth">
            <div class="small-4 medium-1 columns hide-for-small">
                <jdoc:include type="modules" name="redes-top" style="xhtml" />
            </div>
            <div class="small-8 medium-3 columns">
                <a href="index.php"><img class="logo" src="templates/<?php echo $this->template; ?>/img/logo.png" /></a>
            </div>
            <div class="small-4 medium-8 columns">
                <div class="row info_empresa hide-for-small">
                    <div class="small-10 columns"><jdoc:include type="modules" name="info-top" style="xhtml" /></div>
                    <div class="small-2 columns"><jdoc:include type="modules" name="widgets-top" style="xhtml" /></div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <div id="top-menu-resp" class="menu-resp">
                            <div>Menu</div>
                            <a href="#"></a>
                        </div>
                    </div>
                </div>
                <div class="row" id="header-top">
                    <div class="small-12 columns">
                        <jdoc:include type="modules" name="top-menu" style="xhtml" />
                    </div>
                </div>
            </div>            
        </div>
    </header>
    <div class="row fullWidth collapse relativo">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="banner" style="xhtml" />
        </div>
        <div id="modulo-flotante" class="hide-for-small">
            <div>
                <jdoc:include type="modules" name="modulo-flotante" style="xhtml" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="sub-banner" style="xhtml" />
        </div>
    </div>
    <div class="row fullWidth collapse">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="contenido" style="xhtml" />
        </div>
    </div>
    <jdoc:include type="modules" name="contenido-2" style="xhtml" />
    <div class="row">
        <div class="small-12 columns">
            <jdoc:include type="message" />
        </div>
    </div>
    <?php        
        $menu = $app->getMenu();
        if ($menu->getActive() != $menu->getDefault()) : ?>
        <div class="row">
            <div class="large-12 columns component">
                <jdoc:include type="component" />
            </div>
        </div>
    <?php endif; ?>
    <div class="row fullWidth collapse">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="banner-2" style="xhtml" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="sub-banner-2" style="xhtml" />
        </div>
    </div>   
    <footer>
        <div class="info">
            <div class="row">
                <div class="small-12 medium-4 columns medium-text-center">
                    <jdoc:include type="modules" name="footer1" style="xhtml" />
                </div>
                <div class="small-12 medium-4 columns medium-text-center">
                    <jdoc:include type="modules" name="footer2" style="xhtml" />
                </div>
                <div class="small-12 medium-4 columns medium-text-center">
                    <jdoc:include type="modules" name="footer3" style="xhtml" />
                </div>
            </div>
        </div>
        <div class="copy">
            <div class="row">
                <div class="small-12">Copyright &copy; 2015 LuloDSGN SAS </div>
            </div>
        </div>
    </footer>
</body>
</html>
